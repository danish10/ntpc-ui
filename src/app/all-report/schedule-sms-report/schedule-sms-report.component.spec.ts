import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleSmsReportComponent } from './schedule-sms-report.component';

describe('ScheduleSmsReportComponent', () => {
  let component: ScheduleSmsReportComponent;
  let fixture: ComponentFixture<ScheduleSmsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleSmsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleSmsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
