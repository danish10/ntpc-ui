import { Component, ViewChild, OnInit } from '@angular/core';
import { GetService } from '../../services/get.service';
import { PostService } from '../../services/post.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { Department } from '../../shared/department';
import { DatePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { error } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { empty } from 'rxjs/Observer';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';
import { SelectionModel } from '@angular/cdk/collections';
import {FormsModule,FormGroup,FormControl,Validators} from '@angular/forms';
import { AppComponent } from '../../app.component';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-schedule-sms-report',
  templateUrl: './schedule-sms-report.component.html',
  styleUrls: ['./schedule-sms-report.component.css']
})
export class ScheduleSmsReportComponent implements OnInit {

  public loginId:any={};
  public allscheduleSms: any = [];
  public scheduledsms:any;
  public userList:any[];
  public isAdminTrue:boolean=true;
  public allUser:any;
  public user:any={};
  myModel: any;
  public numberOfData: number;
  limit: number;
  page: number = 1;
  filter:any={};

//paginatio ===================================
pageNumber:any=1;
totalPage:any;
size:any=10;
totalData:any;
totalNumberOfData:any;
pageInfo:any={};

//paginatio ===================================
 
  public deleteField: boolean;
  public authenticationError: boolean;
  public noscheduledsms:boolean;

  constructor(private router: Router,private getService: GetService, private modelService: NgbModal, private postService: PostService,
    private itemDataServiceService: ItemDataServiceService, private spinner: NgxSpinnerService,private app:AppComponent) {
    }

  ngOnInit() {
 //   this.getAllScheduledSmsReport();
    this.loginId = this.itemDataServiceService.getUser();
    this.pageInfo.userId=this.loginId.loginId;

    this.allUser='all'+this.loginId.loginId;
    this.user=this.loginId;
    this.pageInfo.userId='all'+this.user.loginId;
    this.showDatabyuser();
    this.getuserList();
    this.pageInfo.size=10;
    this.setPagePrev(1,"");
  }

  getAllScheduledSmsReport(){
    this.app.checkCredential();
    
    this.getService.getScheduledReportList().subscribe(
      data => {
        this.allscheduleSms = data;
        
        if(this.allscheduleSms == 0){
          this.noscheduledsms = true
      }
        this.numberOfData = this.allscheduleSms.length;
        this.limit = this.allscheduleSms.length;
      
      },
      errorCode => {
       
      }
    );
  }

  open(scheduledsms,deleteModal) {
    
    this.scheduledsms = scheduledsms;
  
     this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
 }

 closeModel() {
  this.allscheduleSms 
  this.myModel.close();

}

  deleteMessage(scheduledsms){
   
      this.getService.deleteScheduldesms(scheduledsms.id).subscribe(
        data => {
          
          setTimeout(() => {
            this.closeModel();
          
          }, 500);
          setTimeout(() => {
           
            this.deleteField = true;
            this.authenticationError = false;
          }, 500);
  
          this.getSheduleReport();
         
          setTimeout(() => {
           
            this.deleteField = false;
          }, 3000);
  
        },
        error => {
       
           
          setTimeout(() => {
            this.spinner.hide();
            this.closeModel();
            this.deleteField = false;
            this.authenticationError = true;
  
          }, 2000);
  
  
          setTimeout(() => {
            this.authenticationError = false;
          }, 2000);
      
        }
      );
    
    
  }


  //paginatio ===================================

setPageNext(i, event: any) {
 // debugger;
  //this.currentIndex = this.page + 1;
  if(i<this.totalPage)
  {
    event.preventDefault();
    this.pageInfo.pageNumber=i+1;
    this.getSheduleReport();
  }else{
    event.preventDefault();
    this.pageInfo.pageNumber=i;
    this.getSheduleReport();
  }
 
}
setPagePrev(i, event: any) {
  //
//  this.currentIndex = this.page - 1;
if(i>1){
  event.preventDefault();
  this.pageInfo.pageNumber=i-1;
  this.getSheduleReport();
}else{
  this.pageInfo.pageNumber=1;
  this.getSheduleReport();
}
  
}
//paginatio ===================================



  getSheduleReport(){
   
    this.getService.getTotalScheduleReportListPage(this.pageInfo)
    .subscribe(data => {
         this.allscheduleSms=data.schdeulDataList;
         this.totalPage=data.totalNumberOfPage;
        this.totalNumberOfData=data.totalNumberOfData;
        this.numberOfData= this.allscheduleSms.length;
        this.limit=this.allscheduleSms.length;

        
        setTimeout(() => {
          this.spinner.hide();
    
        }, 2000);
        
          this.pageNumber=data.pageNumber;
           this.size=data.pageSize;
          
    },error=>{
   
      setTimeout(() => {
        this.spinner.hide();
  
      }, 2000);
    });
   
  }


  getuserList() {
    this.app.checkCredential();
    this.getService.getUserList().subscribe((userList) => {
      this.userList = userList;
  
    })
  }

  changeLoginId(){
    this.spinner.show();
    this.pageInfo.userId=this.filter.loginId;
    this.getSheduleReport();
    setTimeout(() => {
      this.spinner.hide();
     
    }, 2000);
   
  }


  showDatabyuser(){
    if(this.loginId.userType == "user")
    {
      this.isAdminTrue=false;
    }else{
      this.isAdminTrue=true;
    }
  }



  refeshPage(){
    this.getuserList();
    this.filter.mobile='';
    this.filter.loginId="";
    this.pageInfo.userId='all'+this.loginId.loginId;
    this.setPagePrev(1,"");
  }
  // refeshPage(){
  //   //console.log("working");
  //   this.getuserList();
  //   this.d.dateFrom='';
  //   this.d.dateTo='';
  //   this.filter.loginId="";
  //   this.pageInfo.userId='all'+this.loginId.loginId;
  //   this.setPagePrev(1,"");
  // }

}
