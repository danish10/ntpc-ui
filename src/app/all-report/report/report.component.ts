import { Component, ViewChild, OnInit } from '@angular/core';
import { error } from 'selenium-webdriver';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { NgForm } from '@angular/forms';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material'
import { Http, Headers } from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';
import { timeout } from 'q';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  instanceMessageReportView: boolean = false;
  instanceVoiceReportView: boolean = false;
  scheduleMessageReportView: boolean = false;
  scheduleVoiceReportView: boolean = false;
  smsTrunkReportView: boolean = false;

  public loginId: any = {};
  public isAdminTrue:boolean=false;
  public instanceMessageList: any = [];
  public scheduleMessageList: any = [];
  public allUser:any;
  
  //public filter: any = {};
  public noContent: boolean;
  reportType: any;
  public IMReport: any;
  public reportList: any = [];
  public dateFrom: any;
  public dateTo: any;
  public d: any = {};
  public noReportCreated: boolean;
  public filterData: any = [];
  public userList:any=[];
  public user:any={};
  // public numberOfData:any;


// pagination pro

  
  pageNumber:any=1;
  totalPage:any;
  totalNumberOfData:any;
  size:any=10;
  totalData:any;

  pageInfo:any={
    pagaNumber:'1',
    size:'10',
    userId:''
  };

//pagination prop




  public name = 'Angular v4 - Applying filters to *ngFor using pipes';
  public numberOfData: number;
  public limit: number;
  public page: number = 1;
  public filter: any = {};
  public true_1: any = 'red';
  public false: any = 'green';
  // displayedColumns = ['reportid','mobile', 'reason','response','status','scheduleDate', 'sendBy'];
  // dataSource = new MatTableDataSource();


  //DataSource;
  //  @ViewChild(MatPaginator) paginator: MatPaginator;
  //  @ViewChild(MatSort) sort: MatSort;

  constructor(private spinner: NgxSpinnerService, iconRegistry: MatIconRegistry, private postService: PostService, private getService: GetService, private itemDataServiceService: ItemDataServiceService,private app:AppComponent) { }

  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.allUser='all'+this.loginId.loginId;
    this.user=this.loginId;
    this.pageInfo.userId='all'+this.user.loginId;
    this.getInstanceMessageRepoart();
    this.showDatabyuser();

    // this.getScheduleMessageReport();
 // this.getTotalReportByPagination();
  //  this.getTotalReport();
    this.getuserList();
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
this.setPagePrev(1,"");
  }

//pagination prop

setPageNext(i, event: any) {
 //debugger;
  //this.currentIndex = this.page + 1;
  if(i<this.totalPage)
  {
    event.preventDefault();
    this.pageInfo.pageNumber=i+1;
    this.getTotalReportByPagination();
  }else{
    event.preventDefault();
    this.pageInfo.pageNumber=i;
    this.getTotalReportByPagination();
  }
 
}
setPagePrev(i, event: any) {
  
// this.currentIndex = this.page - 1;
if(i>1){
  event.preventDefault();
  this.pageInfo.pageNumber=i-1;
  this.getTotalReportByPagination();
}else{
  this.pageInfo.pageNumber=1;
  this.getTotalReportByPagination();
}
  
}


//pagination prop






  getuserList() {
    this.app.checkCredential();
    this.getService.getUserList().subscribe((userList) => {
      this.userList = userList;
     ////console.log(this.userList);
    })
  }

  getTotalReport() {
    this.app.checkCredential();

    // this.spinner.show();
    this.getService.getTotalReportList(this.user.loginId).subscribe(data => {
      ////console.info("data", data);
      this.reportList = data;
      this.noReportCreated = false;
      if (this.reportList < 1) {
        this.noReportCreated = true;
      } else {
        this.noReportCreated = false;
        this.numberOfData = this.reportList.length;
        this.limit = this.reportList.length;
      }

    })
  }



  getReportList() {
    this.app.checkCredential();
    //console.info(this.d);
    this.getService.getReportByDate(this.d)
      .subscribe(data => {
        ////console.info(data);
        //console.info("status", data.status);
        this.reportList = data;
       
          this.numberOfData = this.reportList.length;
          this.limit = this.reportList.length;
         
        // //console.info("list ", this.reportList);
      })
    //  alert("hello");
  }



  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    // this.dataSource.filter = filterValue;
  }

  onSubmit(filter: NgForm) {

    ////console.log(filter  .value);
  }



  reportfind(reportData: any) {

    this.instanceMessageReportView = false;
    this.instanceVoiceReportView = false;
    this.scheduleMessageReportView = false;
    this.scheduleVoiceReportView = false;
    this.smsTrunkReportView = false;

    this.reportType = reportData;
    ////console.log(" report data "+this.reportType)

    switch (this.reportType) {

      case "":
        this.instanceMessageReportView = false;
        this.instanceVoiceReportView = false;
        this.scheduleMessageReportView = false;
        this.scheduleVoiceReportView = false;
        this.smsTrunkReportView = false;
        break;

      case "IMReport":
        ////console.log(" report data "+this.reportType)  
        this.instanceMessageReportView = true;
        break;

      case "IVReport":
        this.instanceVoiceReportView = true;
        break;
      case "SMReport":
        this.scheduleMessageReportView = true;
        break;

      case "SVReport":
        this.scheduleVoiceReportView = true;
        break;

      case "MTReport":
        this.smsTrunkReportView = true;
        break;

    }
  }

  getInstanceMessageRepoart() {
    this.getService.getInstanceMessageRepoart().subscribe(data => {
      this.instanceMessageList = data;
      ////console.log(this.instanceMessageList);
    }), (error => {

      ////console.log("error "+error);
    })

  }

  getScheduleMessageReport() {
    this.getService.getScheduleMessageReport().subscribe(data => {
      this.scheduleMessageList = data;
      ////console.log(this.scheduleMessageList);

    }), (error => {
      ////console.log("scheule data retrive error "+error);
    });
  }

 
  showDatabyuser(){
    if(this.loginId.userType == "user")
    {
      this.isAdminTrue=false;
    }else{
      this.isAdminTrue=true;
    }
  }


  //pagination Report 

  getTotalReportByPagination(){
    //debugger;
    // this.pageInfo.pageNumber=1;
    // this.pageInfo.size=10;
   // this.spinner.show();
    this.getService.getTotalReportListPage(this.pageInfo,this.d)
    .subscribe(data => {
         this.reportList=data.reportDataList;
        // this.contactList = data.numberOfData;
         this.totalPage=data.totalNumberOfPage;
        this.totalNumberOfData=data.totalNumberOfData;
        this.numberOfData= this.reportList.length;
        this.limit=this.reportList.length;

        //  //  this.noContactCreated = false;
        setTimeout(() => {
        //  this.spinner.hide();
    
        }, 2000);
         
          this.pageNumber=data.pageNumber;
           this.size=data.pageSize;
          
    },error=>{
      setTimeout(() => {
       // this.spinner.hide();
  
      }, 2000);
    });
   
  }

  changeLoginId(){
    this.spinner.show();
    this.pageInfo.userId=this.filter.loginId;
    
    console.log("login id ",this.filter.loginId)
    this.getTotalReportByPagination();
    setTimeout(() => {
      this.spinner.hide();
     
    }, 2000);
   
  }


  refeshPage(){
    //console.log("working");
    this.getuserList();
    this.d.dateFrom='';
    this.d.dateTo='';
    this.filter.loginId="";
    this.pageInfo.userId='all'+this.loginId.loginId;
    this.setPagePrev(1,"");
  }
}

//id: 0, pageNumber: 1, pageSize: 10, totalNumberOfPage: 2, totalNumberOfData: 20