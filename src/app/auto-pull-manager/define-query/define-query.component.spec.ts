import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineQueryComponent } from './define-query.component';

describe('DefineQueryComponent', () => {
  let component: DefineQueryComponent;
  let fixture: ComponentFixture<DefineQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
