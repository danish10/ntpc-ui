import { Component, OnInit, ViewChild } from '@angular/core';
import {GetService} from '../../services/get.service';
import {PostService} from '../../services/post.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import {NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';



 


@Component({
  selector: 'app-latestnews',
  templateUrl: './latestnews.component.html',
  styleUrls: ['./latestnews.component.css']
})
export class LatestnewsComponent implements OnInit {


  public admin:any =[
    {
      newsTitle:''
    }
  ]
  @ViewChild('myForm') form: any;
  public loginId:any={};
  public newsList:any;
  public noNewsCreated:boolean=false;
  numberOfData: number;
  //public newsTitle:any={};
  public news:any={};
  myModel:any;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public deleteField: boolean;

  constructor(
    private getService:GetService,
    private postService:PostService,
    private modelService:NgbModal,
    private itemDataServiceService:ItemDataServiceService,
    private spinner: NgxSpinnerService,
    private app:AppComponent
  ) { }

  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.getNewsList();
  }


  getNewsList() {
    this.app.checkCredential();
    this.getService.getNewsList().subscribe(
      data =>{
        ////console.log("*************" , data);
        this.newsList = data;
        this.noNewsCreated=false;
        if(this.newsList<1)
        {
          this.noNewsCreated=true;
        }
        else{
        this.numberOfData = this.newsList.length;
        this.noNewsCreated=false;
      }
      }
    )
  }

  addNews() {
    //debugger;
    let newValue = {
      newsTitle:''
    }
    this.admin.push(newValue);
  }

  

  removeNews(index:any){
    //debugger;
    this.admin.splice(index,1);
  }

  saveNews(){
    this.spinner.show();
    this.admin;
    this.admin.loginId = this.loginId.loginId;
    ////console.log("******************" ,this.admin);
    this.postService.saveLatestNews(this.admin).map((response) => response).subscribe(
      data =>{
        this.getNewsList();
        if (this.form.valid) {
          this.form.reset();
        }
        if (data.status == 200) {
          this.spinner.hide();
          this.successField = true;
          this.authenticationError = false;
        }
        setTimeout(() => {
          this.successField = false;
        }, 2000);
      },
        error => {
          setTimeout(() => {
            this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;
          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }
     
  

  open(news,deleteModal){
    this.news = news;
    this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
  }
 
  deleteNews(news){
    this.getService.deleteNews(news.newsId).map((response) => response).subscribe(
      data => {
        ////console.log(data);
        setTimeout(() => {
          this,this.closeModel();
        },500);
        setTimeout(() => {
         this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);
        this.getNewsList();
        setTimeout(() => {
          this.closeModel();
          this.deleteField = false;
        }, 4000);
      },
        error => {
          setTimeout(() => {
          this.closeModel();
            this.deleteField = false;
            this.authenticationError = true;
          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }

  OpenModel(news,updateModal){
    this.news = news;
    this.myModel=this.modelService.open(updateModal , { windowClass: 'custom-class' });
  }


  closeModel(){
    this.myModel.close();
  }


  updateNews(){
    
    this.postService.updateNews(this.news).map((response) => response).subscribe(
      data => {
        this.closeModel();
        this.getNewsList();
      }
    )

  }
  
}
