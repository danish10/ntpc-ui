import { Component, OnInit } from '@angular/core';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemDataServiceService } from '../../services/item-data-service.service';

@Component({
  selector: 'app-smtp-mail-conf',
  templateUrl: './smtp-mail-conf.component.html',
  styleUrls: ['./smtp-mail-conf.component.css']
})
export class SmtpMailConfComponent implements OnInit {

  public mail:any={
    id:'',
    smtpServer:'',
    smtpPort:'',
    senderMailId:'',
    senderPassword:'',
    subject:'',
    loginId:{}

  };
  public mailList:any=[];
  public loginId:any={};

  public getAllMailConfig:boolean=false;
  
  constructor(private itemDataServiceService: ItemDataServiceService,private getService:GetService,private postService:PostService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.loginId=this.itemDataServiceService.getUser();
    this.getMailConfiguration();
    
  }

  getMailConfiguration(){
    this.getService.getMailConfigurationList().map((response)=>response)
    .subscribe(data=>{
        this.mailList=data;
        //console.log("==== mail =====",this.mailList)
        this.mail={};
    },error=>{
        //console.log("==== error ======== ",error)
    })
  }

  onSubmit(){
this.mail.loginId=this.loginId;
    this.postService.saveSmtpMailConfiguration(this.mail).map((response)=>response)
    .subscribe(data=>{
        //console.log(data.status);
        this.getAllMailConfig=true;
        this.getMailConfiguration();
      },error=>{
        //console.log("error ",error);
      }
    )

  }
  update(mail){
    this.mail=mail;
  }

  onUpdate(){
    
    this.mail.loginId=this.loginId;
        this.postService.updatSmtpMailConfiguration(this.mail).map((response)=>response)
        .subscribe(data=>{
            //console.log(data.status);
            this.getMailConfiguration();
          },error=>{
            //console.log("error ",error);
          }
        )
    
      }

      delete(id){
        this.getService.deletemailconfiguration(id).subscribe(data=>{
            if(data==200){
              this.getMailConfiguration();
            }
        },error=>{

        })
      }


}
