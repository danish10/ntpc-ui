import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmtpMailConfComponent } from './smtp-mail-conf.component';

describe('SmtpMailConfComponent', () => {
  let component: SmtpMailConfComponent;
  let fixture: ComponentFixture<SmtpMailConfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmtpMailConfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmtpMailConfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
