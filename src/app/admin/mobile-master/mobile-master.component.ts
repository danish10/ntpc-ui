import { Component, OnInit } from '@angular/core';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { Http, Response,RequestOptions,Headers} from '@angular/http';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '../../app.component';


@Component({
  selector: 'app-mobile-master',
  templateUrl: './mobile-master.component.html',
  styleUrls: ['./mobile-master.component.css']
})
export class MobileMasterComponent implements OnInit {

  private txtFile:any;

  public fileUploadReport:any=[];
  public uploadFileType1:any;
  public uploadFileType2:any;
  public isTextFile:boolean;
  public errorTXTFileMessage:any;
  public errorEXFileMessage:any;
  public errorTXTFileMessage1:any;
  public errorEXFileMessage2:any;
  public isExcelFile:boolean;
  public file:any={};
  public fileupload:boolean=false;
  private size:any;
  private picSize:any;
  private messageType:any;
  public authenticationError: boolean;
  
  public showUpdate:boolean=true;
  public successContact:boolean=false;
  public successMessage:any;
  public data:any;
  public successLocation:boolean=false;
  public successDepartment:boolean=false;
  public Error:boolean;
  public authentication;boolean;
  
  public user:any = {

  }
  public loginId:any={};
  
  constructor(private modalService:NgbModal,private getService:GetService,private postService:PostService,private http:Http,
    private itemDataServiceService:ItemDataServiceService,private spinner: NgxSpinnerService,private app:AppComponent) { }

  ngOnInit() {
    this.loginId=this.itemDataServiceService.getUser();
   
  
    this.app.checkCredential();
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

 
 
  onTextFileChange(event){
   
    this.isTextFile=true;
    let fileList:FileList = event.target.files;
    this.uploadFileType1="txt";
    if(fileList.length>0){
      
      if(fileList && event.target.files[0]){
        let file:File = fileList[0];
       
        if(file.type == "text/plain") {
          ////console.log(file.type);
         // alert("this is text file");
          this.user.mobileTextDocument = file;
          this.errorTXTFileMessage="";

        }
        else {
          this.isTextFile=false;
         this.errorTXTFileMessage="only .txt file allow";
           alert("please fill right file");
         
        }

      }

    }
  }

  onXlsFileChange(event){
    this.isExcelFile=true;
    this.uploadFileType2="excel";
    let fileList:FileList = event.target.files;
    if(fileList.length>0){

      if(fileList && event.target.files[0]){
        let file:File = fileList[0];
      
        //  alert("this is excel file");
          this.user.mobileTextDocument = file;
          
          if((file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (file.type=="application/vnd.ms-excel")) {
            ////console.log(file.type);
           // alert("this is text file");
            this.user.mobileTextDocument = file;
            this.errorEXFileMessage="";
  
          }
          else {
            this.isExcelFile=false;
            this.errorEXFileMessage="only .xlsx or .xls file allow";
             alert("please fill right file");
          }
      
      } else {
        this.isExcelFile=false;
        this.errorEXFileMessage="only .xlsx or .xls file allow";
       
      
     }

    }
  }


  uploadTextFile(value){
    if((value=="2" && this.isExcelFile==true) || (value=="1" && this.isTextFile==true)){
    this.spinner.show();
    ////console.log(this.messageType)
     this.postService.uploadTextDocument(this.user.mobileTextDocument , this.loginId.loginId,this.messageType)
     .map((response) =>response)
     .subscribe(data =>{
     ////console.log("data ",data);

     ////console.log("datastatus ",data.status);
      if(data.status==200){
        this.spinner.hide();
        this.fileupload=true;
        this.getReport();
        this.user.xlsFile="";
        this.user.txtFile="";
        this.authenticationError = false;
      }
        ////console.log("Upload Doucument ####" , data);
        setTimeout(() => {
          this.fileupload = false;

        }, 2000);
      },
        error => {
          setTimeout(() => {
            this.spinner.hide();
            this.fileupload = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 2000);

        });
  }else{
    if(value==1){
      this.isTextFile=false;
      this.errorTXTFileMessage="please upload .txt file";
    }else{
      this.isExcelFile=false;
      this.errorEXFileMessage="please upload  .xlsx or .xls file";
    }
      
  }
}

reset(value){
if(value==1){
  this.user.txtFile="";
  this.errorTXTFileMessage="";
}else{
  this.user.xlsFile="";
  this.errorEXFileMessage="";
}
  
  
}

getReport(){
  
  this.getService.getFileUploadRepot()
  .subscribe(data=>{
  //  //console.log("report "+data);
    this.fileUploadReport=data;
    // //console.log("report "+this.fileUploadReport[0]);
    // //console.log("report "+this.fileUploadReport[1]);
    // //console.log("report "+this.fileUploadReport[2]);
  })
}
// ---------------------------------------------UPDATE PHONE FROM SERVER---------------------------


updatePhoneDirectory(){
 
 this.spinner.show();
  this.getService.getdatafromserver().subscribe(data=>{
    console.log(data);

  if(data>0)
  {
    this.successContact=true;
    this.spinner.hide();
    this.successMessage=data;
    this.authenticationError = false;
  }
  setTimeout(()=>{
    this.successContact=false;
},5000 );
  }, error => {
    setTimeout(() => {
      this.spinner.hide();
      this.successContact = false;
      this.authenticationError = true;

    }, 2000);
    setTimeout(() => {
      this.authenticationError = false;
    }, 4000);

  });
}


updateLocation(){
 this.spinner.show();
  this.getService.getdatafromserverlocation()
  .subscribe(data=>{
    if(data>0)
    {
      this.successLocation=true;
      this.spinner.hide();
      this.successMessage=data;
      this.Error=false;
    }
    setTimeout(()=>{
      this.successLocation=false;
  },5000 );
    },error=>{
      setTimeout(() => {
        this.spinner.hide();
        this.successLocation = false;
        this.Error = true;
  
      }, 2000);
      setTimeout(() => {
        this.Error = false;
      }, 4000);
  
    });
  }
     
    

updateDepartment(){
  this.spinner.show();
  this.getService.getdatafromserverDepartment()
  .subscribe(data=>{
    if(data>0)
    {
      this.successDepartment=true;
      this.spinner.hide();
      this.successMessage=data;
      this.authentication=false;
    }
    setTimeout(()=>{
      this.successDepartment=false;
     
  },5000 );
    },error=>{
      setTimeout(() => {
        this.spinner.hide();
        this.successDepartment = false;
        this.authentication = true;
  
      }, 2000);
      setTimeout(() => {
        this.authentication = false;
      }, 4000);
  
    });
  }

updateOn(value)
{
 if(value=='1'){
  this.showUpdate=true;
  
 }
 if(value=='2'){
  this.showUpdate=false;
  
 }
}
}