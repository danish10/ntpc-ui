import { Component, OnInit } from '@angular/core';
import { Account } from '../../account';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { GetService } from '../../services/get.service';
import { PostService } from '../../services/post.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-account-status',
  templateUrl: './account-status.component.html',
  styleUrls: ['./account-status.component.css']
})
export class AccountStatusComponent implements OnInit {

  public account: any = {};
  //public account = new Account;
  public URL: any;
  public urlName:any;
  public forSms: any;

  public isStart: boolean = false;
  public isStop: boolean = false;
  public disabled: boolean = false;
  public disabled1:boolean;

  public urlnamestatus:boolean=false;

  public urlList: any = [];
  public url: any = {};
  public myModel: any;

  public successField: any = false;
  public authenticationError: boolean;

  public startReponse: any;
  public stopReponse: any;

  public URL_RESPONSE: boolean[];
  public SMS_APPLICATION_RESPONSE: boolean[];

  public start: boolean;

  public loginId: any = {};
  

  constructor(private modalService: NgbModal,private itemDataServiceService: ItemDataServiceService, private getService: GetService, private postService: PostService, private spinner: NgxSpinnerService,private app:AppComponent ) { }

  openLg(url,updateModal) {
    this.disabled=true;
    this.url = url;
    ////console.log("Open Modal For----" ,this.url);
    this.myModel = this.modalService.open(updateModal, { size: 'lg' });
  }

  closeModel() {
    this.myModel.close();
  }


  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.getReponseURL();
    this.startService();
    this.stopService();
   // this.getsmsApplicationStatus();
    this.getURL();
  }

  getsmsApplicationStatus() {
    this.getService.getSmsApplicationReponse().map((response) => response).subscribe(
      data => {
        this.SMS_APPLICATION_RESPONSE = data;
        ////console.log("SMS_APPLICATION_RESPONSE-----", this.SMS_APPLICATION_RESPONSE);
      }
    )
  }

  getReponseURL() {
    this.app.checkCredential();
    this.getService.getUrlReponse().map((response) => response)
      .subscribe(data => {
        this.URL_RESPONSE = data;
        ////console.log("URL_RESPONSE-----", this.URL_RESPONSE);
      })
  }

  getURL() {
   
    this.getService.getURLList()
      .subscribe(data => {
        this.urlList = data;
       //console.log("Current URL Working is----", this.urlList);
      },
      errorCode => {
        ////console.log("Error Code #### " ,errorCode);
      }
    )
  }

 

  // getURL() :void{
  //   this.getService.getURLList().subscribe(
  //     data =>{
  //       this.urlUsing = data;
  //       ////console.log("----Current URL Working is----" ,this.urlUsing);
  //     },
  //     error =>{
  //       ////console.log("Error in Fetch URL" ,error)
  //     }
  //   );
  // }
 
  editURL(){
    this.disabled = false;
    this.disabled1=true;
  }

  updateURL(){
    // 
    this.disabled1=false;
  // this.spinner.show();
    ////console.log("############################" ,this.url)
    this.postService.updatesmsUrl(this.url).map((response) => response).subscribe(data =>{
      ////console.log("##################" ,data);
      if (data.status == 200) {
        this.getURL();
        //this.spinner.hide();
          this.successField = true;
          this.authenticationError = false;
      }   setTimeout(() => {
        this.successField = false;
        this.closeModel();
      }, 1000);

    },
    error => {
      setTimeout(() => {
       // this.spinner.hide();
        this.successField = false;
        this.authenticationError = true;

      }, 1000);


      setTimeout(() => {
        this.authenticationError = false;
        this.closeModel();
      }, 1000);

    }
  );
    
  }

  // ********** For Start Service OF URL And GATEWAY **********
  // submitForm(value: any):void{
  //  
  //   ////console.log(value);
  //   this.forSms = value.forSms;
  //   if(this.forSms=="URL")
  //   {
  //     ////console.log("Welcome To:" +this.forSms);
  //     this. [disabled]="isStart" = true;
  //        this.postService.startURLService().map((response) => response)
  // .subscribe(data => {

  // })

  //   }
  // }
 

  startService() {
  
    ////console.log("Account Data----", this.account);
    if (this.account.smsService == undefined) {
      this.start = true;

    } else {

      if (this.account.smsService == 'URL') {
        ////console.log("----START", this.account.smsService, "FOR FIRE SMS----")

        ////console.log("----", this.account.smsService, "IS SELECT FOR START SMS SERVICE----")
        this.account.loginId = this.loginId;
      
        // var myString: string = <string><any> this.account.URL;
      
        this.postService.startURLService(this.account)
        .subscribe(data => {
          ////console.log("DATA_START", data._body);
            this.startReponse = data._body;
            ////console.log("Start Service Reponse", this.startReponse);
            if (this.startReponse == '') {
            
              this.urlnamestatus=true;
            } 
            else{
              this.urlnamestatus=false;
            }
            
          },error => {
            
          })

        this.getReponseURL();
      }
      else {
        this.start;
        ////console.log("----START", this.account.smsService, "FOR FIRE SMS----")
        ////console.log("----", this.account.smsService, "IS SELECT FOR START SMS SERVICE----")
        
        
      }
    }

  }

  stopService() {
    
    ////console.log("Account Data----", this.account);
    if (this.account.smsService == undefined) {
      this.start = true;

    } else {
      
      if (this.account.smsService == "URL") {
        ////console.log("----STOP", this.account.smsService, "FOR FIRE SMS----")
        ////console.log("Account Data----", this.account);
        this.account.loginId = this.loginId;
        ////console.log("----", this.account.smsService, "IS SELECT FOR STOP SMS SERVICE----")

        this.postService.stopURLService(this.account).map((response) => response)
          .subscribe(data => {
            //this.startReponse=true;
            ////console.log("DATA_STOP", data);
            this.stopReponse = data;
            ////console.log("Stop Service Reponse", this.stopReponse);
          })
        this.getReponseURL();

      }
      else {
        this.start;
        ////console.log("----STOP", this.account.smsService, "FOR FIRE SMS----")
        ////console.log("----", this.account.smsService, "IS SELECT FOR STOP SMS SERVICE----")
      }
    }
  }

}
