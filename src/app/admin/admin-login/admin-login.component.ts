import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { User } from '../../shared/user';
import { PostService } from '../../services/post.service';
import { GetService } from '../../services/get.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { AppComponent } from '../../app.component';
@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  public authenticationError: boolean;
  public successField: boolean;
  public user: any = {};
  public loginId: any = {};
  public mobile: any = {};
  public allLoginDetails: any = [];
  public forgetpasswordPage: boolean = false;
  public loginpage: boolean = true;
  public newsList: any;
  public wrongId: boolean;
  public wrongMobile: boolean
  numberOfData: number;
  public isCredential:boolean=false;
  
  public admin: any = {};
  // loginId:any='';
  // password:any='';
  WIDTH: any = 800;
  HEIGHT: any = 505;

  images: any = [
    { item: 2 }, { item: 3 }, { item: 4 }, { item: 5 }, { item: 6 }, { item: 7 }, { item: 8 }
  ]

  constructor(private router: Router, private postService: PostService,
    private itmService: ItemDataServiceService, private getService: GetService,private app:AppComponent
  ) { }

  ngOnInit() {
    
    localStorage.clear();
    this.loginId = this.getService.getUserStatus();
    this.getLoginCount();
    this.getNewsList();
   
    // this.ntpcRunningStatus();
  }



  ntpcRunningStatus() {
    //   debugger
    this.getService.getNtpcApplicationReponse().map((response) => response).subscribe(
      data => {
        // //console.log("NTPC_APPLICATION_RESPONSE-----", data);
      }
    )
  }


  getLoginCount(): void {
    this.getService.getDetails().subscribe(
      data => {
        this.allLoginDetails = data;
        ////console.log("----AdminLogin LoginCount----" ,this.allLoginDetails);
      }
    );
  }

  login() {
    this.app.checkCredential();
    this.admin;
   
    this.postService.doLogin(this.admin).map((response) => response.json()).subscribe(
      data => {
        this.user = data;
        ////console.log(this.user);

        if (this.user != null) {
          this.itmService.setUser(this.user);
          this.router.navigate(['/home'])
        }
        else {

          this.router.navigate(['/'])

          this.successField = false;
          this.authenticationError = true;
          setTimeout(() => {
            this.authenticationError = false;
          }, 5000);

        }
      });
  }

  forget() {
    this.admin
    this.postService.forgetPasword(this.admin).map((response) => response)
      .subscribe(data => {
        if (data.status == 200) {
          this.successField = true;
          this.authenticationError = false;
        }
        setTimeout(() => {
          this.successField = false;
        }, 2000);
      },
        error => {
          if (error.status == 400) {
            this.wrongId = true;
            this.authenticationError = false;
          }
          setTimeout(() => {
            this.wrongId = false;
          }, 2000);
          if (error.status == 404) {
            this.wrongMobile = true;
            this.authenticationError = false;
          }
          setTimeout(() => {
            this.wrongMobile = false;
          }, 2000);

        });
  }


  showForgetPage() {
    this.forgetpasswordPage = true;
    this.loginpage = false;
  }

  loginPage() {
    this.forgetpasswordPage = false;
    this.loginpage = true;
  }

  getNewsList() {
    this.getService.getNewsList().subscribe(
      data => {
        ////console.log("*************", data);
        this.newsList = data;
        this.numberOfData = this.newsList.length;
      }
    )
  }


  restrictNumeric(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

 
}


