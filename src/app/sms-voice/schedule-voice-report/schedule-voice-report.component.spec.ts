import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleVoiceReportComponent } from './schedule-voice-report.component';

describe('ScheduleVoiceReportComponent', () => {
  let component: ScheduleVoiceReportComponent;
  let fixture: ComponentFixture<ScheduleVoiceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleVoiceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleVoiceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
