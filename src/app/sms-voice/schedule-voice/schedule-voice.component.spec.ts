import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleVoiceComponent } from './schedule-voice.component';

describe('ScheduleVoiceComponent', () => {
  let component: ScheduleVoiceComponent;
  let fixture: ComponentFixture<ScheduleVoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleVoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleVoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
