import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientIdMasterComponent } from './client-id-master.component';

describe('ClientIdMasterComponent', () => {
  let component: ClientIdMasterComponent;
  let fixture: ComponentFixture<ClientIdMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientIdMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientIdMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
