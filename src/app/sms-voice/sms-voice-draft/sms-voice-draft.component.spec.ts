import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsVoiceDraftComponent } from './sms-voice-draft.component';

describe('SmsVoiceDraftComponent', () => {
  let component: SmsVoiceDraftComponent;
  let fixture: ComponentFixture<SmsVoiceDraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsVoiceDraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsVoiceDraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
