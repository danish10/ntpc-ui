import { Component, OnInit } from '@angular/core';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-schedule-message',
  templateUrl: './schedule-message.component.html',
  styleUrls: ['./schedule-message.component.css']
})
export class ScheduleMessageComponent implements OnInit {

  public phoneDirctoryList:any[];
  public contactGroupList:any[];
  public departmentList:any[];
  public smsList:any[];
  public sms:any=[{
    phoneDirectorys:[],
    contactGroups:[],
    departments:[],
    mobile:'',
    messageName:'',
    message:'',
    scheduleType:''

}];

public instance: any={
  mobile:'',
  smsName:'',
  smstxt:'',
  phoneDirectoryName:'',
  contactGroupName:'',
  departMentName:'',
  scheduleType:'',
  scheduleDate:''

};

public scheuleTypeList:any[]=[
  {
      id: 1,
      lable:'One Time',
      description: 'ONETIME'
  },
  {
      id: 2,
      lable:'Daily',
      description: 'DAILY'
  }, {
    id: 3,
    lable:'Weekly',
    description: 'WEEKLY'
},{
    id:4,
    lable:'Monthly',
    description:'MONTHLY'

},{
    id: 5,
    lable:'Yearly',
    description: 'YEARLY'
}
  
]

public noContactCreated:boolean=false;
  constructor(private getService:GetService,private postService:PostService) { }

  ngOnInit() {
    this.getPhoneDircatoryList();
    this.getContactGrouplist();
    this.getSmsList();
    this.getAlldepartment();
  }


  getPhoneDircatoryList() {

    this.getService.getContactList().subscribe(data => {
      this.phoneDirctoryList = data;
      this.noContactCreated = false;

      ////console.log(data)
     ////console.log("phoneDirctoryList :"+this.phoneDirctoryList);
      
        if (this.phoneDirctoryList.length < 1) {
          this.noContactCreated = true;
        }
      },
      error => {
        // this.alertService.error(error);
    //    this.loading = false;
      });
  }


  getContactGrouplist()
  {
    this.getService.getGrouplist().subscribe(data => {
      this.contactGroupList = data;
      ////console.log(data)
      ////console.log("contactGroupList :"+this.contactGroupList);
      
      },
      error => {
        ////console.log(error);
      });
  }

  getSmsList(){
    
       this.getService.getSmsList().subscribe((data)=>{
           this.smsList=data;
           ////console.log(this.smsList)
           ////console.log("sms list"+data);
       },(error)=>{
         ////console.log(error);
       });
   }

   getAlldepartment() {
    this.getService.getDepartmentList().subscribe(
      data => {
        this.departmentList = data;
        ////console.log("Fetch All department :  ", data);
      },
      errorCode => {
       
        ////console.log(errorCode);
      }
    );
  }

  onSelectionChange(entry) {
    this.sms.scheduleType = entry;
}

saveScheduleMessage(){
  
 
  ////console.log(this.sms);
  this.instance.mobile=this.sms.mobile;
  this.instance.smsName=this.sms.messageName;
  this.instance.smstxt=this.sms.message;
  this.instance.phoneDirectoryName=this.sms.phoneDirectory;
  this.instance.contactGroupName=this.sms.contactGroup;
  this.instance.departMentName=this.sms.departments;
  this.instance.scheduleDate=this.sms.scheduleDate;
 this.instance.scheduleType=this.sms.scheduleType.description;

  this.postService.saveScheduleMessage(this.instance).map((response)=>response)
  .subscribe(data=>{
    ////console.log("save instance message",data);
  
    ////console.log(data);
    if(data.status == 201){
   

  }  
}); 

}

}
