import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVoiceFileComponent } from './upload-voice-file.component';

describe('UploadVoiceFileComponent', () => {
  let component: UploadVoiceFileComponent;
  let fixture: ComponentFixture<UploadVoiceFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadVoiceFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVoiceFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
