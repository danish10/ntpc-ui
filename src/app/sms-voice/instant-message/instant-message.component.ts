import { Component, OnInit, Type } from '@angular/core';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { AppComponent } from '../../app.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-instant-message',
  templateUrl: './instant-message.component.html',
  styleUrls: ['./instant-message.component.css']
})
export class InstantMessageComponent implements OnInit {



  public yyyy: any = new Date().getUTCFullYear();
  public MM: any = new Date().getMonth();
  public dd: any = new Date().getDate();
  public hh: any = new Date().getHours();
  public mm: any = new Date().getMinutes();
  public min = new Date();
  public max = new Date(2100, 3, 21, 20, 30)
  //  public scheduleDate  =  new Date(this.yyyy,this.MM,this.dd,this.hh,this.mm);
  //  public min = new Date(this.yyyy,this.MM,this.dd,this.hh,this.mm);
  //  public max = new Date(2100, 3, 21, 20, 30);

  public instant: any = {
    mobile: '',
    smsName: '',
    smsText: '',
    location: '',
    grade:'',
    phoneDirectoryName: '',
    contactGroupName: '',
    departmentName: '',
    priority: '',
    scheduleDate: '',
    scheduleType: '',
    uploadContactList: ''

  };
  public strDate: any;
  public isExcelFile: any = false;
  public myModel: any;
  public errorEXFileMessage: any;
  public uploadContactList: any = [];
  public xlsFile: File;
  public isDataSlect: boolean = false;
  public dateMessage: any = "";
  public errorbordersms: any = "";
  public isContactSelect: boolean = false;
  public isMessageSelect: boolean = false;
  public URL_RESPONSE: boolean[];
  public phoneDirctoryList: any[];
  public contactGroupList: any[];
  public departmentList: any[];
  public duplicatList: any = [];
  public saveSmsList = new Map<Type<string>, Observable<any>>();
  public contactGroup: any = "contactGroup";
  public department: any = "departments";
  public grade: any ="grades";
  public phoneDirectory: any = "phoneDirectory";
  public message_: any = "message";
  public messageName_: any = "messageName";
  public mobile_: any = "mobile";
  public loginId: any = {};
  public phoneDirectorys: any[];
  public contactGroups: any[];
  public departments: any[];
  public mobile: any;
  public messageName: any;
  public projects: any = [];
  public grades:any =[];
  public message: any;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public deleteField: boolean;
  public chooseOne: boolean;
  public requiredMsg: string;
  public requiredMsg1: string;
  public requiredMsgSms: string;
  public messageSelect: boolean;
  public required: string;
  public errorborder: any;
  public scheduleType: any = "ONETIME";
  // public smsPriority:any="LOW";
  public disabled: boolean = true;
  public firstAttempt: boolean;
  public secondAttempt: boolean;
  public successFile: any = false;
  public totalContactFile: any = false;

  public text_max: any = 150;
  public text_length: any;
  public text_remaining: any;
  public sms: any = {};

  // {
  //   phoneDirectorys: [],
  //   contactGroups: [],
  //   departments: [],
  //   locations: [],
  //   projects: [],
  //   mobile: '',
  //   messageName: '',
  //   message: '',
  //   priority: '',
  //   scheduleDate: '',
  //   scheduleType: ''


  // }];


  public locationList: any[];
  public gradeList: any[];
  //public departmentList:any[];
  public projectList: any[];
  public subGradeList: any[];

  public smsList: any[];
  public noContactCreated: boolean = false;
  constructor(private itemDataServiceService: ItemDataServiceService, private getService: GetService, private postService: PostService,
    private spinner: NgxSpinnerService, private app: AppComponent, private router: Router, private modalService: NgbModal) {

  }
  public items: any[];
  ngOnInit() {

//this.getAlldepartment();
    this.min = new Date(this.yyyy, this.MM, this.dd, this.hh, this.mm);
    this.text_max = 150;
   // this.getPhoneDircatoryList();
   
   // this.getSmsList();
   
    
    this.loginId = this.itemDataServiceService.getUser();
    this.getReponseURL();
    this.getContactGrouplist();
    this.getAllValueList();
  }

  getReponseURL() {
    this.getService.getUrlReponse().map((response) => response)
      .subscribe(data => {
        this.URL_RESPONSE = data;
      })
  }

  getPhoneDircatoryList() {
    this.app.checkCredential();
    this.getService.getContactList().subscribe(data => {
      this.phoneDirctoryList = data;
      this.noContactCreated = false;
      if (this.phoneDirctoryList.length < 1) {
        this.noContactCreated = true;
      }
    },
      error => {

      });
  }


  getContactGrouplist() {
    this.getService.getGrouplist().subscribe(data => {
      this.contactGroupList = data;
    },
      error => {

      });
  }

  getSmsList() {
    this.getService.getSmsList().subscribe((data) => {
      this.smsList = data;
    }, (error) => {

    });
  }

  getAlldepartment() {
    this.getService.getDepartmentList().subscribe(
      data => {
        this.departmentList = data;
      },
      errorCode => {
      }
    );
  }


  reset() {
    this.disabled = true;
    this.scheduleType= "ONETIME";
  }


  // saveInstaceMessage() {
  //     this.instant.locationName
  //     this.instant.mobile = this.sms.mobile;
  //     this.instant.smsName = this.sms.messageName;
  //     this.instant.smsText = this.sms.message;
  //     this.instant.phoneDirectoryName = this.sms.phoneDirectory;
  //     this.instant.contactGroupName = this.sms.contactGroup;
  //     this.instant.departmentName = this.sms.departments;
  //     this.instant.locationName = this.sms.locations;
  //     this.instant.projectName = this.sms.projects; 
  //     this.instant.gradeName = this.sms.grades; 
  //     this.instant.substGradeName = this.sms.substGrades;
  //     this.setDateFormat(this.sms.scheduleDate);
  //     this.instant.scheduleDate=this.strDate;
  //     this.instant.scheduleType = this.scheduleType;
  //     this.instant.loginId = this.loginId;
  //     this.instant.uploadContactList = this.uploadContactList;

  //     this.postService.saveScheduleMessage(this.instant).map((response) => response)
  //       .subscribe(data => {
  //         this.duplicatList = data.json();
  //         setTimeout(() => {
  //           this.closeModel();
  //           this.disabled = true;
  //         }, 200);
  //         if (data.status == 200) {
  //           this.successField = true;
  //           this.authenticationError = false;
  //           this.sms.mobile = '';
  //           this.sms.message = '';
  //           this.sms.scheduleDate = '';

  //         }
  //         setTimeout(() => {
  //           this.successField = false;
  //         }, 3000);

  //       }, error => {

  //         console.log(" error "+error);
  //         setTimeout(() => {
  //           this.successField = false;
  //           this.authenticationError = true;
  //           this.closeModel();
  //         }, 2000);
  //         setTimeout(() => {
  //           this.authenticationError = false;
  //         }, 4000);
  //       });
  //   }
  // }



  // doCount() {
  //   this.text_length=this.sms.message.length;
  //   this.text_length=this.text_max-this.text_length;

  // }

  // public scheduleTypes: any = [
  //   {id:0 , type:"ONETIME" , value:"ONETIME"},
  //   {id:0 , type:"DAILY" , value:"DAILY"},
  //   {id:0 , type:"WEEKLY" , value:"WEEKLY"},
  //   {id:0 , type:"MONTHLY" , value:"MONTHLY"},
  //   {id:0 , type:"YEARLY" , value:"YEARLY"}

  // ]


  public scheduleTypes: any = [

    { id: 0, scheduleType: "ONETIME", value: "ONETIME" },
    { id: 1, scheduleType: "DAILY", value: "DAILY" },
    { id: 2, scheduleType: "WEEKLY", value: "WEEKLY" },
    { id: 3, scheduleType: "MONTHLY", value: "MONTHLY" },
    { id: 4, scheduleType: "YEARLY", value: "YEARLY" }
  ]

  public smsPriorities: any = [


    { id: 0, priority: "MEDIUM", value: "MEDIUM" },
    { id: 1, priority: "LOW", value: "LOW" },
    { id: 2, priority: "HIGH", value: "HIGH" }

  ]

  // checkRequired() {

  //   this.chooseOne = false;

  //   if (this.sms.contactGroup == undefined && this.sms.departments == undefined && this.sms.mobile == undefined && this.sms.locations == undefined && this.sms.projects == undefined && this.sms.grades == undefined && this.sms.substGrades == undefined && this.uploadContactList.length == 0) {

  //     this.chooseOne = true;
  //     this.requiredMsg1 = "Please select one from above Boxes";
  //     this.firstAttempt = false;


  //   }
  //   else {
  //     this.errorborder = '1px solid green';
  //     this.chooseOne = false;
  //     this.messageSelect = false;
  //     this.firstAttempt = true;
  //   }
  //   if (this.sms.messageName == undefined && this.sms.message == undefined) {
  //     this.errorbordersms = '1px solid red';
  //     this.messageSelect = true;
  //     this.requiredMsg = "Required"
  //     this.secondAttempt = false;

  //   } else {
  //     this.errorbordersms = '1px solid green';
  //     this.chooseOne = false;
  //     this.messageSelect = false;
  //     this.secondAttempt = true;

  //   }

  //   if (this.firstAttempt == true && this.secondAttempt == true) {
  //     this.disabled = false;
  //   } else {

  //   }

  // }
  onSearchChange(searchValue: string) {
    ////console.log(searchValue);
  }
  restrictNumeric(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  onXlsFileChange(event) {
    // this.isExcelFile=true;
    // this.uploadFileType2="excel";
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {

      if (fileList && event.target.files[0]) {
        let file: File = fileList[0];

        //  alert("this is excel file");
        this.sms.xlsFile = file;

        if ((file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (file.type == "application/vnd.ms-excel")) {
          ////console.log(file.type);
          // alert("this is text file");
          this.sms.xlsFile = file;
          this.isExcelFile = false;
          this.errorEXFileMessage = "";
          this.postService.uploadContactNumber(this.sms.xlsFile, this.loginId.loginId).
            subscribe(data => {
              //alert("success");
              // //console.info(data);
              if (data.status == 200) {
                this.successFile = true;
                // this.totalContactFile=false;
              }
              setTimeout(() => {
                this.successFile = false;
                //this.totalContactFile=false;
              }, 5000);
              this.totalContactFile = true;
              this.chooseOne = false;
              this.requiredMsg = "";
              this.errorborder = '1px solid green';
              this.uploadContactList = data.json();
            }, error => {
              ////console.info("ddddddddddddddddddddd  ",error);
            });
        }
        else {
          this.isExcelFile = true;
          this.errorEXFileMessage = "only .xlsx or .xls file allow";
          alert("please fill right file");
        }

      } else {
        this.isExcelFile = true;
        this.errorEXFileMessage = "only .xlsx or .xls file allow";
      }

    }
  }
  // open(sms, sendModel) {
  //   this.sms = sms;
  //   this.myModel = this.modelService.open(sendModel, { windowClass: 'custom-class ' });
  // }

  openLg(sms, sendModel) {
    this.sms = sms;
    this.myModel = this.modalService.open(sendModel, { size: 'lg' });
  }
  closeModel() {
    this.myModel.close();

  }

  setDateFormat(value) {
    //debugger;

    if (value == undefined || value == "" || value == null) {
      value = new Date();
      this.strDate = "" + value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate() + " " + value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds() + "";
    } else {
      this.strDate = "" + value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate() + " " + value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds() + "";
    }
    ////console.log(value);

    ////console.info(this .strDate)
  }


  getAllValueList() {
    //departmentName substGradeName gradeName locationName projectName
    this.getService.getAllValueForInstanctBox()
      .subscribe(data => {
        this.locationList = data.locationName;
        this.departmentList = data.departmentName;
        this.projectList = data.projectName;
        this.gradeList = data.grade;
        this.subGradeList = data.substGradeName;
      }, error => {


      })


  }


  messageDetail(sms) {
    if (this.sms.contactGroup == undefined  && this.sms.mobile == undefined && this.sms.locations == undefined && this.sms.projectName == undefined && this.sms.grades == undefined  && this.uploadContactList.length == 0) {

      this.chooseOne = true;
      this.requiredMsg = "Please select one from above Boxes";
      this.errorborder = '1px solid red';

    }
    else {
      this.requiredMsg = " ";
      this.chooseOne = false;
    }

    // this.messageSelect = false;
    // this.requiredMsg = "";
    // this.errorborder='1px solid green';
    if (this.sms.messageName == undefined && this.sms.message == undefined) {

      this.messageSelect = true;
      this.requiredMsgSms = "message required"
      this.errorbordersms = '1px solid red';
    } else {
      //   this.chooseOne = false;
      this.messageSelect = false;
      //   this.errorborder='1px solid green';
      this.errorbordersms = '1px solid green';
      //    }
    }
    
    if (this.chooseOne == false && this.messageSelect == false) {
      sms.scheduleType = this.scheduleType;
      if(this.scheduleType == null){
        sms.scheduleType="ONETIME";
      }
      this.itemDataServiceService.saveMessageData(sms);
     
      this.router.navigate(['/message/detail'])
    }
  }
  changeCheck(sms: string) {

      this.getService.getProjectRelated(sms).subscribe(data => {
      this.locationList = data;
     
      this.projects = sms;
    }, error => {
    })
  }

  locationCheck(sms) {

    if (this.projects.length >= '0') {
      this.projects = 'NULL';
    }
    this.getService.getLocationRelated(this.projects, sms).subscribe(data => {
      this.gradeList = data;
    }, error => {
    })
  }

  checkRequired() {

    this.chooseOne = false;

    if (this.sms.contactGroup == undefined  && this.sms.mobile == undefined && this.sms.locations == undefined && this.sms.projectName == undefined && this.sms.grades == undefined  && this.uploadContactList.length == 0) {

      this.chooseOne = true;
      this.requiredMsg1 = "Please select one from above Boxes";
      this.firstAttempt = false;


    }
    else {
      this.errorborder = '1px solid green';
      this.chooseOne = false;
      this.messageSelect = false;
      this.firstAttempt = true;
    }
    if (this.sms.messageName == undefined && this.sms.message == undefined) {
      this.errorbordersms = '1px solid red';
      this.messageSelect = true;
      this.requiredMsg = "Required"
      this.secondAttempt = false;

    } else {
      this.errorbordersms = '1px solid green';
      this.chooseOne = false;
      this.messageSelect = false;
      this.secondAttempt = true;

    }

    if (this.firstAttempt == true && this.secondAttempt == true) {
      this.disabled = false;
    } else {

    }

  }


}




