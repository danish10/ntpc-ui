import { Component, group } from '@angular/core';
import { GetService } from './services/get.service';
import { ItemDataServiceService } from './services/item-data-service.service';
import { RouterModule, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //title = 'Ntpc Messaging Portal';

  public loginId:any={};
  public applicationRunning:any={};
  public isValue:boolean=false;
  public now: Date = new Date();
  public appRunning:boolean;
  


  constructor(private getService:GetService,private itemDataServiceService:ItemDataServiceService,private router: Router ){
    setInterval(() => {
      this.now = new Date();
    }, 1);
  }
  ngOnInit(){
    this.getService.getIp();
    this.loginId=this.itemDataServiceService.getUser();
    this.checkCredential();

    
    
  
    ////console.log("user  home ---name ",this.loginId)
  }
  

  // getAlldepartment():void {
  //   this.getService.getDepartmentList().subscribe(
  
  //     data => {
  //      this.allDepartment= data;
  //      // this.dataSource.data = data;
  //       this.nodepartmentCreated = false;
  //       ////console.log("Fetch All department :  ", this.allDepartment);
  //       ////console.log('this.department.length=' + this.allDepartment.length);
  //       this.numberOfData = this.allDepartment.length;
  //       this.limit = this.allDepartment.length;
  //      // ////console.log('this.department.length = ' + this.dataSource.data.length);
  //       if (this.allDepartment < 1) {
  //         this.nodepartmentCreated = true;
  //       }
  //     }
  //     ,
  
  //     errorCode => {
  //       ////console.log("Error Code #### " ,errorCode);
  //     });
  // }


  // getIp(){
  //   this.getService.getJSON().subscribe(data => {
  //       this.obj = data;
  //       console.log("$$$$$$$$$$$$" , this.obj);
  //   })
  // }

  checkCredential(){

    this.getService.getCheckCredential()
    .subscribe(data => { 
      this.applicationRunning =  data;
     ////console.log("#### CREDENTIAL ####" ,this.applicationRunning)
     
      if (this.applicationRunning.result) {
       
      } else {
        this.router.navigate(['/error'])
       
      }
     
    },error=>{
     ////console.log("#### CREDENTIAL error ####" ,this.applicationRunning)
    });
    
  }
  
  
}
