export class Department {

    departmentId:number;
    departmentName:any;
    departmentDesc:any;
    createdBy:any;
    createdDate:any;

    constructor(departmentId,departmentName,departmentDesc,createdBy,createdDate){
        this.departmentId=departmentId;
        this.departmentName=departmentName;
        this.departmentDesc=departmentDesc;
        this.createdBy=createdBy;
        this.createdDate=createdDate;
    }


}
