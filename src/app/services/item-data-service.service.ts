import { Injectable } from '@angular/core';
import {RouterModule,Router} from '@angular/router';

@Injectable()
export class ItemDataServiceService {

  public locationList : any [];
  public departmentList:any [];
  public sharingContact: any;
  public sharingMessageDetail:any;
  public user : any= {};
  public key:any;
  public baseUrl:any;
  public smsUrl:any;
  public isCredential:boolean=false;
  
  public scheduleTypes: any = [

    { id: 0, scheduleType: "ONETIME", value: "ONETIME" },
    { id: 1, scheduleType: "DAILY", value: "DAILY" },
    { id: 2, scheduleType: "WEEKLY", value: "WEEKLY" },
    { id: 3, scheduleType: "MONTHLY", value: "MONTHLY" },
    { id: 4, scheduleType: "YEARLY", value: "YEARLY" }
  ]



  constructor(private router:Router) { }

  public setLocationList(location){
    ////console.log("set location "+location)
    this.locationList=location;
  }
  public getLocationList(){
    ////console.log("get location "+this.locationList)
    return this.locationList;
  }


  public setDepartmentList(department){
    ////console.log("set department "+department)
    this.departmentList=department;
  }
  public getDepartmentList(){
    ////console.log("get department "+this.departmentList)
    return this.departmentList;
  }


  saveContactData(str) {
    this.sharingContact = str;
}

public setUser(user){ 
     
    localStorage.setItem(this.key,JSON.stringify(user));
    ////console.log("Session created..........",localStorage)
 
  
  
}

public getUser(){
  let user;
  ////console.log("user data item ",this.user);
  if(localStorage.length == 0){
    this.router.navigate(['']);
  }
  return JSON.parse(localStorage.getItem(user));
}

setBaseurl(baseUrl){
//  console.log(" set base user",baseUrl)
  this.baseUrl=baseUrl;
}
getBaseurl(){
 // console.log(" set base user",this.baseUrl)
  return this.baseUrl;
}

setSmsurl(smsUrl){
 // console.log(" set sms user",smsUrl)
  this.smsUrl=smsUrl;
}
getSmsurl(){
  //console.log(" get smsm user",this.smsUrl)
  return this.smsUrl;
}

saveMessageData(str) {
  this.sharingMessageDetail = str;
 
  
}
getMessageData() {
  return this.sharingMessageDetail;
 

}
}
