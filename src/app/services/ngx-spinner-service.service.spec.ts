import { TestBed, inject } from '@angular/core/testing';

import { NgxSpinnerServiceService } from './ngx-spinner-service.service';

describe('NgxSpinnerServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgxSpinnerServiceService]
    });
  });

  it('should be created', inject([NgxSpinnerServiceService], (service: NgxSpinnerServiceService) => {
    expect(service).toBeTruthy();
  }));
});
