import { Injectable } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SELECT_VALUE_ACCESSOR } from '@angular/forms/src/directives/select_control_value_accessor';
import { ItemDataServiceService } from './item-data-service.service';


@Injectable()
export class GetService {
  private ip: any[];
  private myIP: any;
  private myPort: any;
  private myapp: any;

  private baseUrl = "";
  private smsUrl = "";

  // private baseUrl = "http://localhost:8080/NTPC_APP";
  // private smsUrl = "http://localhost:8080/NTPC_SMS";
  public loginId: any = {};
  public obj: any = {};
  constructor(private http: Http, private router: Router, private itemDataServiceService: ItemDataServiceService) { }

  ngOnInit() {

    this.getIp();
  }

  getIp() {

    return this.http.get("./assets/data.json").map((response) => response.json())
      .subscribe(data => {
        this.ip = data;
        this.myIP = this.ip[0].ip;
        this.myPort = this.ip[0].port;
        this.myapp = this.ip[0].app;
        if (this.myIP == "localhost") {
          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        } else {

          this.baseUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        }
        this.itemDataServiceService.setBaseurl(this.baseUrl);

        this.myIP = this.ip[1].ip;
        this.myPort = this.ip[1].port;
        this.myapp = this.ip[1].app;
        if (this.myIP == "localhost") {
          this.smsUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        } else {
          this.smsUrl = 'http://' + this.myIP + ':' + this.myPort + '/' + this.myapp;
        }

        this.itemDataServiceService.setSmsurl(this.smsUrl);
        // console.info("ip..............",data);
        // console.info("ip........base ......",this.baseUrl);
        // console.info("ip..........sms....",this.smsUrl);

      });
  }

  getCheckCredential() {
    this.getIp();

    //console.log(" creaadebtuib ",this.baseUrl)
    this.itemDataServiceService.getBaseurl;
    return this.http.get(this.baseUrl + "/index/check/credential")
      .map((response) => response.json());
  }





  getUserStatus() {
    return this.http.get(this.baseUrl + '/index/loginstatus')
      .map((response) => response.json());
  }

  // getUserStataus(){
  //   this.getUserStatus()
  //   .map((response)=>response)
  //   .subscribe(data =>{
  //     this.loginId=data;
  //     //console.log(" user Home details ",data.status);
  //     //console.log("user   Home login ",data);
  //   },error =>{
  //     this.router.navigate(['/']);
  //     //console.log("user  Home error ",error);
  //   })

  //   return this.loginId;
  // }


  getLocationList() {
    return this.http.get(this.baseUrl + '/location/locationlist')
      .map((response) => response.json());
  }

  deleteLocation1(value: any): Observable<any> {

    return this.http.delete(this.baseUrl + "/location/deletelocation/" + value)
      .map(success => success.status);
  }


  getUserList() {
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + '/user/userlist?loginId=' + this.loginId.loginId)
      .map((response) => response.json());
  }

  deleteUser(value: any): Observable<any> {

    return this.http.delete(this.baseUrl + "/user/deleteuser/" + value).map(success => success.status);
  }

  getSmsList() {
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + '/sms/smslist?loginId=' + this.loginId.loginId)
      .map((response) => response.json());
  }

  getVoiceSmsList() {
    return this.http.get(this.baseUrl + '/sms/voicesmslist')
      .map((response) => response.json());
  }



  deleteSMS(value: any): Observable<any> {

    return this.http.delete(this.baseUrl + "/sms/deletesms/" + value).map(success => success.status);
  }


  //=========================================================

  getContactList() {
    this.getIp();
    debugger;
    console.log('base url',this.baseUrl);
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + '/phonedirectory/phoneDirectorylist?loginId=' + this.loginId.loginId)
      .map((response) => response.json())
  }

  getContactList1(value,filter) {
    debugger;
    console.log(value,filter)
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + '/page?pageNumber=' + value.pageNumber + '&size=' + value.size +'&loginId=' + this.loginId.loginId + '&value=' + filter.fieldValue + '&field=' + filter.field)
      .map((response) => response.json())
  }


  // getContactList1(page: number) {
  //   return this.http.get(this.baseUrl + '/phonedirectory/phoneDirectorylist?page=' + page)
  //     .map((response) => response.json())

  // }



  removeContact(contactId: string): Observable<number> {

    return this.http.delete(this.baseUrl + "/phonedirectory/phoneDirectory/" + contactId).map(success => success.status)
  }

  getGrouplist() {
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + '/contactgroup/grouplist?loginId=' + this.loginId.loginId)
      .map((response) => response.json())
  }



  getGroupListpagination(value) {
    return this.http.get(this.baseUrl + "/contact/group?pageNumber=" + value.pageNumber + "&size=" + value.size + "&loginId=" + value.userId + "&groupName=" + value.groupName)
      .map((response) => response.json());

  }

  getAllgrouplist() {
    return this.http.get(this.baseUrl + '/allcontact/allgrouplist')
      .map((response) => response.json())
  }

  getOneGroupContactList(value: any): Observable<any> {

    return this.http.get(this.baseUrl + '/allcontact/ongroupcontact/' + value)
      .map((response) => response.json());
  }

  getContactListNotInGroup(value: any): Observable<any> {
    this.loginId = this.itemDataServiceService.getUser();

    return this.http.get(this.baseUrl + '/allcontact/showallcontact/' + value + '?loginId=' + this.loginId.loginId)
      .map((response) => response.json());
  }


  // let headers = new Headers();
  // headers.append('enctype' , 'multipart/form-data');
  // let options = new RequestOptions({ headers : headers});
  // let formData = new FormData();
  // formData.append("uploadTextDocument", docName);
  // //console.log("####### uploadTextDocument #########" , docName);
  // formData.append("loginId" , loginId);
  // formData.append("messageType",messageType)
  // //console.log("####### loginId #########" , loginId);

  removeContactFromGroup(value1: any, value2: any): Observable<any> {

    let headers = new Headers();
    headers.append('enctype', 'multipart/form-data');
    let options = new RequestOptions({ headers: headers });

    let formData = new FormData();
    formData.append("groupId", value2);
    formData.append("phoneDirectoryId", value1);

    return this.http.post(this.baseUrl + '/allcontact/removecontactfromgroup', formData, options)

  }
  removeGroup(contact_groupId: string): Observable<number> {
    return this.http.delete(this.baseUrl + "/contactgroup/contactGroup/" + contact_groupId)
      .map(success => success.status)
  }

  //Fetch All Department
  getDepartmentList(): Observable<any[]> {
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + "/department/all/department?loginId=" + this.loginId.loginId).map((response) => response.json());

  }


  // getDepartmentList(): Observable<any[]> {
  //   //this.loginId=this.itemDataServiceService.getUser();
  //   return this.http.get(this.baseUrl + "/all/department").map((response) => response.json());

  // }

  //Delete Department
  deleteDepartment(departmentId: string): Observable<number> {
    return this.http.delete(this.baseUrl + "/department/department/" + departmentId).map(success => success.status)
  }



  //get LoginDetails
  getDetails() {
    return this.http.get(this.baseUrl + "/index/all/loginDetails").map((response) => response.json());
  }

  getloginFail(value: any): Observable<any> {
    return this.http.get(this.baseUrl + "/index/login/failure/" + value).map((response) => response.json());
  }

  getloginSuccess(value: any): Observable<any> {
    return this.http.get(this.baseUrl + "/index/login/success/" + value).map((response) => response.json());
  }

  getlastloginSuccess(value: any): Observable<any[]> {
    return this.http.get(this.baseUrl + "/index/last/login/success/" + value).map(res => res.text() ? res.json() : res);
  }

  getlastloginFail(value: any): Observable<any[]> {
    return this.http.get(this.baseUrl + "/index/last/login/fail/" + value).map(res => res.text() ? res.json() : res);
  }

  getInstanceMessageRepoart() {
    return this.http.get(this.baseUrl + "/intancemessage/instancemsglist").map((response) => response.json());
  }

  getScheduleMessageReport() {
    return this.http.get(this.baseUrl + "/schedulemsglist").map((response) => response.json());
  }

  //Fetch All Report
  getReportList(loginId: any): Observable<any[]> {
    return this.http.get(this.baseUrl + "/report/all/Report?loginId=" + loginId).map((response) => response.json());

  }
  //Fetch All Scheduled Report
  getScheduledReportList(): Observable<any[]> {
    this.loginId = this.itemDataServiceService.getUser();
    return this.http.get(this.baseUrl + "/smsschedule/smsschedulelist?loginId=" + this.loginId.loginId).map((response) => response.json());

  }

  getMailConfigurationList(): Observable<any[]> {
    return this.http.get(this.baseUrl + "/allconfig").map((response) => response.json());

  }
  deletemailconfiguration(value: any) {
    return this.http.delete(this.baseUrl + "/deleteconfig/" + value).map(success => success.status);
  }



  getUrlReponse(): Observable<any[]> {
    return this.http.get(this.smsUrl + "/status/url").map(res => res.text() ? res.json() : res);
  }

  getSmsApplicationReponse(): Observable<any[]> {
    return this.http.get(this.smsUrl + "/health").map(res => res.text() ? res.json() : res);
  }

  getNtpcApplicationReponse(): Observable<any[]> {
    //debugger
    return this.http.get(this.baseUrl + "/health/").map(res => res.text() ? res.json() : res);
  }

  getTotalReportList(loginId: any): Observable<any[]> {
    return this.http.get(this.baseUrl + "/report/all/Report?loginId=" + loginId).map((response) => response.json());

  }


  //Pagination Report 
  getTotalReportListPage(value1, value2): Observable<any> {
    //debugger;
    return this.http.get(this.baseUrl + "/report?pageNumber=" + value1.pageNumber + "&size=" + value1.size + "&loginId=" + value1.userId + "&dateFrom=" + value2.dateFrom + "&dateTo=" + value2.dateTo)
      .map((response) => response.json());

  }

  getReportByDate(value: any): Observable<any> {
    ////console.log("get  service ",value);
    return this.http.get(this.baseUrl + "/report/report/bydate/?dateFrom=" + value.dateFrom + "&dateTo=" + value.dateTo + "").map((response) => response.json());
  }


  deleteScheduldesms(id: string): Observable<number> {
    return this.http.delete(this.baseUrl + "/smsschedule/delete/scheduled/sms" + id).map(success => success.status);
  }

  getURLList() {

    return this.http.get(this.baseUrl + "/user/url/list").map((response) => response.json());
  }


  getAllMobileList(mobile: any) {

    // return this.http.get(this.baseUrl+"/phonedirectory/mobileList?mobile="+value).map((response)=>response.json());
    return this.http.get(this.baseUrl + '/phonedirectory/mobilenumberexist?mobile=' + mobile + '').map((response) => response.json());
  }
  getEmpIdList(empId: any) {

    // return this.http.get(this.baseUrl+"/phonedirectory/mobileList?mobile="+value).map((response)=>response.json());
    return this.http.get(this.baseUrl + '/phonedirectory/empidexist?empId=' + empId + '').map((response) => response.json());
  }
  getFileUploadRepot() {

    // return this.http.get(this.baseUrl+"/phonedirectory/mobileList?mobile="+value).map((response)=>response.json());
    return this.http.get(this.baseUrl + '/fileupload/file/report').map((response) => response.json());
  }
  getAllUserMobileList(mobile: any) {

    // return this.http.get(this.baseUrl+"/phonedirectory/mobileList?mobile="+value).map((response)=>response.json());
    return this.http.get(this.baseUrl + '/user/usermobilenumberexist?mobile=' + mobile + '').map((response) => response.json());
  }

  deleteNews(value: any): Observable<any> {

    return this.http.delete(this.baseUrl + "/user/delete/news/" + value).map(success => success.status);
  }

  getNewsList(): Observable<any> {
    return this.http.get(this.baseUrl + "/user/news/list").map((response) => response.json());
  }

  getFindPhoneDirectoryOfOtherLocation(value) {
    // let formData = new FormData();
    // formData.append("locationName","Dadri");
    // formData.append("mobile","98656463116");
    return this.http.get(this.baseUrl + "/phonedirectory/otherlocation?locationName=" + value.locationName + "&mobile=" + value.mobile + "&name=" + value.name + "&groupId=" + value.groupId)
      .map((response) => response.json());

  }

  findPhoneDirectory(value) {
     debugger;

    // + value.locationName + "&mobile=" + value.mobile + "&name=" + value.name + "&groupId=" + value.groupId
    return this.http.get(this.baseUrl + "/phonedirectory/findbynameornumber?loginId=" + this.loginId.loginId + "&value=" + value.fieldValue + "&field=" + value.field)
      .map((response) => response.json());

  }


  //paginatiion schedule report
  getTotalScheduleReportListPage(value): Observable<any> {
    return this.http.get(this.baseUrl + "/schedule/report?pageNumber=" + value.pageNumber + "&size=" + value.size + "&loginId=" + value.userId)
      .map((response) => response.json());
  }

  getAllValueForInstanctBox(){
    return this.http.get(this.baseUrl+"/intancemessage/instant/value")
    .map((response) => response.json());
  }

  getdatafromserver(){
    return this.http.get(this.baseUrl+"/server/updatedata")
    .map((response) => response.json());
  }
  getdatafromserverlocation(){
    return this.http.get(this.baseUrl+"/location/updatedata")
    .map((response) => response.json());
  }
  getdatafromserverDepartment(){
    return this.http.get(this.baseUrl+"/department/updatedata")
    .map((response) => response.json());
  }

  getProjectRelated(project: string){
    return this.http.get(this.baseUrl+"/phonedirectory/location/" + project).map((response) => response.json());
  }

  getLocationRelated(projects,location)
  {
   
  return this.http.get(this.baseUrl + '/phonedirectory/grade/' +projects + "/" + location)
  .map((response) => response.json());
  
  }

  removeMessageDetail(id: string,loginId:any){

    return this.http.get(this.baseUrl + "/tempMessage/deleteTempMessage/" + id+"/"+loginId).map((success) => success.json())
  }

  scheduleMessageSave(user,action){  
     
        return this.http.get(this.baseUrl + "/sms/saveschedule/"+user.loginId+"/"+action)
        .map((response) => response)
      }

}