import { TestBed, inject } from '@angular/core/testing';

import { ItemDataServiceService } from './item-data-service.service';

describe('ItemDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemDataServiceService]
    });
  });

  it('should be created', inject([ItemDataServiceService], (service: ItemDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
