import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { User } from '../shared/user';
import { PostService } from '../services/post.service';
import { GetService } from '../services/get.service';
import { ItemDataServiceService } from '../services/item-data-service.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public isSuperAdmin:boolean=false;
  public isAdmin:boolean=false;
  public isUser:boolean=false;
  private addcontactGroupView:boolean;
  private addinstantMessageView:boolean;
  private addmobileMasterView:boolean;
  private addphoneDirectoryView:boolean;
  public issuperAdminTrue:boolean=false;
  public locationName:any;
 
  
  



  constructor(private router:Router,
    private postService: PostService,
    private getService: GetService,
    private itmService: ItemDataServiceService) { }

    private user: any = {};
    public loginId: any = {};
    public userName:any;



  ngOnInit() {
    this.loginId=this.itmService.getUser();
    // this.userName=this.loginId.userName;
    this.showDatabyuser();
    ////console.log("******************************" ,this.loginId);
     this.usermsg();
    


  }

  usermsg(){
 ////console.log("user menu ----details",this.userName);
  }

  logout(){
    this.postService.doLogout(this.loginId).map((response=>response))
    .subscribe(data=>{
      ////console.log("log out successfull "+data);
      localStorage.clear();
     this.router.navigate(['/']);
    },error =>{
      ////console.log("log out error "+error);
    })
    this.loginId={};
    this.router.navigate(['/']);
  }

  // getUserStataus(){
  //   debugger;
  //   this.getService.getUserStatus()
  //   .map((response)=>response)
  //   .subscribe(data =>{
  //     ////console.log(" user getere details ",data);
  //     this.loginId=data;
  //     ////console.log("user login ",this.loginId);
  //   },error =>{
  //     ////console.log("user error ",error);
  //   })
  // }

  // checkUserType(userType){
  
  //   switch(userType){
  //     case "superadmin":
  //       this.isSuperAdmin=true;
  //       this.isAdmin=true;
       
  //     break;

  //     case "admin":
  //     this.isSuperAdmin=true;
  //     this.isAdmin=true;
     
  //     break;
  //    case "user":
  //    this.isSuperAdmin=false;
  //    this.isAdmin=false;
  //    this.isUser=true;
  //    break;
  //   }

  // }
  showDatabyuser(){
    if(this.loginId.userType == "superadmin")
    {
      this.issuperAdminTrue=true;
    }else{
      this.issuperAdminTrue=false;
    }
  }

}
