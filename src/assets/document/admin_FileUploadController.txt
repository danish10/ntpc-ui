package com.yss.ntpc.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yss.ntpc.entity.FileUploadEntity;
import com.yss.ntpc.file.service.FileUploadService;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class FileUploadController {

	
	@Value("${filepath.uploadpath}")
	//private String UPLOADED_FOLDER ="D://NTPC_ALERT_SERVICES//";
	private String UPLOADED_FOLDER;
	

	
	String prefix = "";

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	@Autowired
	private IGenericService<FileUploadEntity> iDataGenericService;


	@RequestMapping("/FileUploadlist")
	public  ResponseEntity<List<FileUploadEntity>> getFileUploadData() {

		logger.info("woriking");
		 List<FileUploadEntity> FileUploadList=iDataGenericService.fetch(new FileUploadEntity());
		return new ResponseEntity<List<FileUploadEntity>>(FileUploadList,HttpStatus.OK);
	}
	
	
	@PostMapping("upload/textFileDocument")
	public ResponseEntity<?> uploadtextFileDocument(@RequestParam(value="loginId") String loginId ,
			@RequestParam(value="uploadTextDocument" ,required=false) MultipartFile uploadTextDocument){
			logger.info("Uplodate Mobile Text Document For : " +loginId);
				uploadMobileDocument(uploadTextDocument,loginId);
				logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@" );
				return new ResponseEntity<>(HttpStatus.OK);
			
	}
	
	@PostMapping("/createFileUpload")
	public ResponseEntity<?> saveFileUpload(@RequestParam(value = "txtFile", required = false) MultipartFile txtFile){
		
//		if(!txtFile.isEmpty()) {
//			String ext=txtFile.getOriginalFilename();
//			String ext2=txtFile.getContentType();
//			String ext3=txtFile.getContentType();
//			File f=new File(ext);
//			//FileUploadService.Read(ext);
//			logger.info(f.getName());
//			logger.info("file save"+ext);
//			try {
//				uploadDocuments(txtFile, "contact");
//			}catch(Exception e) {
//				
//				logger.info("success file uploate"+txtFile);
//			}
//			
//			
//			
//		}
	
		logger.info("success file uploate");
		logger.info("success file uploate"+txtFile);
		//iDataGenericService.save(objFileUpload);
		//logger.info("Data saved successfully !"+objFileUpload);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/updateFileUpload")
	public ResponseEntity<?> updateFileUpload(@RequestBody FileUploadEntity objFileUpload) {
		iDataGenericService.update(objFileUpload);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("/deleteFileUpload")
	public ResponseEntity<?> deleteFileUpload(@RequestParam(value = "FileUploadId", required = true) long FileUploadId) {
		
		logger.info("FileUpload id "+FileUploadId);
		FileUploadEntity objFileUpload = iDataGenericService.find(new FileUploadEntity(), "where FileUploadId = " +  Long.valueOf(FileUploadId));
		if (objFileUpload == null) {
			return new ResponseEntity<>(objFileUpload, HttpStatus.NO_CONTENT);
		}
		
		iDataGenericService.delete(objFileUpload);
		logger.info("Data deleted successfully !");
		return new ResponseEntity<>(HttpStatus.GONE);
	}

	@PostMapping("/findFileUpload/id")
	public ResponseEntity<?> findByFileUploadId(@RequestBody FileUploadEntity objFileUpload) {

		FileUploadEntity FileUpload = iDataGenericService.find(objFileUpload, objFileUpload.getId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(FileUpload, HttpStatus.CREATED);
	}

//	@PostMapping("/findFileUpload/name")
//	public ResponseEntity<?> findByFileUploadame(@RequestBody FileUploadEntity objFileUpload) {
//
//		FileUploadEntity FileUpload = iDataGenericService.find(objFileUpload, objFileUpload.getFileName());
//
//		logger.info("Data saved successfully !");
//		return new ResponseEntity<>(FileUpload, HttpStatus.CREATED);
//	}
	
	
	/****************************************************************************************************************************/
	public void uploadMobileDocument(MultipartFile uploadTextDocument , String loginId) {
	
		FileUploadEntity objfileUpload = new FileUploadEntity();
		
		try {
		
			String MOBILE_DOCUMENT = uploadDocuments(loginId,  uploadTextDocument);
			objfileUpload.setUploadTextDocument(MOBILE_DOCUMENT !=null ? MOBILE_DOCUMENT : null);
			objfileUpload.setLoginId(loginId);
			iDataGenericService.update(objfileUpload);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("######## Error in File Upload ########" + e.getMessage());
		}
	}
	
	
	
	private String uploadDocuments(String loginId ,MultipartFile multipartFile) throws Exception {

		String folder_name = "";
		String fileName = "";
		String fileNameO="";
		String newDocName = "";
		String newPath = "";
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(UPLOADED_FOLDER  + "/");
				
				if (!doc.exists()) {
					logger.info(" Dir " + loginId + " NOT Exists");
					doc.mkdirs();
				}
				

				String s = multipartFile.getOriginalFilename();
				String[] words = s.split("\\s+");
				for (int i = 0; i < words.length; i++) {
					fileName = fileName + words[i].replaceAll(" ", "");
				
					/*mobile.txt*/
					logger.info("-----Trim file name----- " + fileName);
					
				}
				
				folder_name = UPLOADED_FOLDER  + loginId + "/" + fileName;
				
				/* D:/NTPC_ALERT_SERVICES/219/mobile.txt */
				
				logger.info("----- Orignal folder_name :----- " + folder_name);
			
				File file = new File(fileName);
				File filePath = file.getAbsoluteFile();
				System.out.println("------ Get Real Path Where Doc from Hit.. -------- " + filePath); 
				
//				if (fileNamePrefix != null) {
//					newDocName = fileNamePrefix + fileName;
//					logger.info("New Document nmae " + newDocName);
//				} else {
//					newDocName = fileName;
//					logger.info("New Document nmae " + newDocName);
//
//				}
				
				newDocName = fileName;
				logger.info("-----New Document name ---- " + newDocName);
				
				/*  D:/NTPC_ALERT_SERVICES/219/mobile.txt  */
				
				newPath = UPLOADED_FOLDER  + loginId + "/" + newDocName;
				logger.info("-----rename file -----: " + newPath);
				File newFile = new File(newPath);
				logger.info("New File ----- " + newFile);

				if (!file.isDirectory()) {
					file.renameTo(newFile);
				}
				
				  try {
					// Get the file and save it somewhere
					byte[] bytes = multipartFile.getBytes();
					logger.info("----Bytes----" + bytes);
					
					/* D:\NTPC_ALERT_SERVICES\219\mobile.txt */
					
					//Path path = Paths.get(newPath);
					
					Path path = Paths.get(UPLOADED_FOLDER  + loginId + "_" +fileName);
					logger.info("-----path -----" + path);
					
					 Files.write(path, bytes);
					 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.err.println(e);
				}
				
				
				//newDocName=path.toString();
				//logger.info("newDocName After To String -------" + newDocName );
			}
		} catch (Exception e) {
			logger.info("----- IOException ----- : " + e.getMessage());
		}
		
		
		//new FileUploadService().Read();
//		new FileUploadService().Read(fileName);
		logger.info("----- ----- "+ fileName);
		return fileName;
	}

}
